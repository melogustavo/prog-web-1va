import config from 'config';

import ValidateExceptionns from '../exceptions/validate';

module.exports = (app) => {
  const controller = app.controllers.jobOportunitie;
  const validations = app.validations.jobOportunitie;

  const baseURL = `${config.get('base_url')}/job-oportunities`;

  const baseValidationAndControllerCall = async (serviceName, req, res) => {
    const errors = await validations[serviceName](req, res);
    if (errors.length === 0) {
      controller[serviceName](req, res);
    } else {
      res.status(400).send(new ValidateExceptionns(400, 'Erro ao processar solicitação!', req.url, errors));
    }
  };

  app.get(baseURL, (req, res) => {
    controller.findAll(req, res);
  });

  app.get(`${baseURL}/:id`, async (req, res) => {
    baseValidationAndControllerCall('getOneJobOportunitie', req, res);
  });

  app.post(`${baseURL}`, async (req, res) => {
    baseValidationAndControllerCall('createJobOportunitie', req, res);
  });

  app.put(`${baseURL}/:id`, async (req, res) => {
    baseValidationAndControllerCall('updateJobOportunitie', req, res);
  });

  app.delete(`${baseURL}/:id`, async (req, res) => {
    baseValidationAndControllerCall('deleteJobOportunitie', req, res);
  });
};
