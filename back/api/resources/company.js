import config from 'config';

module.exports = (app) => {
  const controller = app.controllers.company;

  const baseURL = `${config.get('base_url')}/companies`;

  app.get(baseURL, (req, res) => {
    controller.findAll(req, res);
  });
};
