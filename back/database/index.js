import Sequelize from 'sequelize';

import databaseConfig from '../config/database';

import Company from '../api/models/Company';
import JobOportunity from '../api/models/JobOportunity';

const models = [Company, JobOportunity];

class DataBase {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    models.forEach((model) => model.init(this.connection));

    models.forEach((model) => {
      model.associate && model.associate(this.connection.models);
    });
  }
}

export default new DataBase();
