import Company from '../models/Company';
import JobOportunity from '../models/JobOportunity';

const allowedFields = ['id', 'name'];

module.exports = () => {
  const controller = {};

  controller.findAll = async (req, res) => {
    const jobOportunities = await JobOportunity.findAll({
      order: [['id', 'ASC']],
      attributes: allowedFields,
      include: [
        {
          model: Company,
          as: 'company',
          attributes: allowedFields,
        },
      ],
    });

    return res.status(200).send(jobOportunities);
  };

  controller.getOneJobOportunitie = async (req, res) => {
    const { id } = req.params;

    const jobOportunitie = await JobOportunity.findByPk(id, {
      attributes: allowedFields,
      include: [
        {
          model: Company,
          as: 'company',
          attributes: allowedFields,
        },
      ],
    });

    return res.status(200).send(jobOportunitie);
  };

  controller.createJobOportunitie = async (req, res) => {
    const { company } = req.body;

    const createdJobOportunitie = await JobOportunity.create({
      ...req.body,
      companyId: company.id,
    });

    return res.status(201).send(createdJobOportunitie);
  };

  controller.updateJobOportunitie = async (req, res) => {
    const { id } = req.params;
    const { company } = req.body;

    const jobOportunity = await JobOportunity.findByPk(id);

    await jobOportunity.update({
      ...req.body,
      companyId: company.id,
    });

    return res.status(204).send();
  };

  controller.deleteJobOportunitie = async (req, res) => {
    const { id } = req.params;

    await JobOportunity.destroy({ where: { id } });

    return res.status(204).send();
  };

  return controller;
};
