import Company from '../models/Company';

const allowedFields = ['id', 'name'];

module.exports = () => {
  const controller = {};

  controller.findAll = async (req, res) => {
    const jobOportunities = await Company.findAll({
      order: [['id', 'ASC']],
      attributes: allowedFields,

    });

    return res.status(200).send(jobOportunities);
  };

  return controller;
};
