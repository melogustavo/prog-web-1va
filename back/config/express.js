const express = require('express');
const bodyParser = require('body-parser');
const config = require('config');
const consign = require('consign');
const cors = require('cors');

require('dotenv/config');

module.exports = () => {
  const app = express();

  if (process.env.API_ADDRESS) {
    const [scheme, host] = process.env.API_ADDRESS.split('://');
    swaggerDocument.host = host;
    swaggerDocument.schemes.unshift(scheme);
  }

  // SETANDO VARIÁVEIS DA APLICAÇÃO
  app.set('port', process.env.PORT || config.get('server.port'));

  // MIDDLEWARES
  app.use(bodyParser.json());

  app.use(cors());

  consign({ cwd: 'api' })
    .then('models')
    .then('validations')
    .then('controllers')
    .then('resources')
    .into(app);


  return app;
};
