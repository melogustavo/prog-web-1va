module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('companies', {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    created_at: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    updated_at: {
      type: Sequelize.DATE,
      allowNull: false,
    },
  }).then(() => queryInterface.bulkInsert('companies',
    [
      {
        name: 'Empresa 1',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Empresa 2',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Empresa 3',
        created_at: new Date(),
        updated_at: new Date(),
      },
    ])),

  down: (queryInterface) => queryInterface.dropTable('companies'),
};
