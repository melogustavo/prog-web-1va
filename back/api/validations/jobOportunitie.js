import * as Yup from 'yup';
import FieldMessage from './fieldmessage';
import Company from '../models/Company';
import JobOportunity from '../models/JobOportunity';

function idValid(id) {
  return !(Number.isNaN(id)) && Number.isInteger(+id);
}

module.exports = () => {
  const validations = {};

  validations.getOneJobOportunitie = async (req) => {
    const errors = [];

    const { id } = req.params;
    if (!idValid(id)) {
      errors.push(new FieldMessage('id', 'Parametro deve ser do tipo inteiro'));
    }

    const jobOportunitie = await JobOportunity.findByPk(id);
    if (!jobOportunitie) {
      errors.push(new FieldMessage('id', 'Oportunidade de emprego inexistente'));
    }

    return errors;
  };

  validations.createJobOportunitie = async (req) => {
    const errors = [];
    const { company } = req.body;

    const schema = Yup.object().shape({
      name: Yup.string().required('Campo Obrigatório'),
      company: Yup.object().shape({
        id: Yup.number().integer().required('Campo Obrigatório'),
      }).required('Campo Obrigatório'),
    });

    try {
      await schema.validate(req.body, { abortEarly: false });
    } catch (err) {
      err.inner.forEach((error) => {
        errors.push(new FieldMessage(error.path, error.message));
      });
      return errors;
    }

    const companyInDatabase = await Company.findByPk(company.id);
    if (!companyInDatabase) {
      errors.push(new FieldMessage('company.id', 'Empresa não existe'));
    }

    return errors;
  };

  validations.updateJobOportunitie = async (req) => {
    const errors = [];
    const { id } = req.params;
    const { company } = req.body;

    if (!idValid(id)) {
      errors.push(new FieldMessage('id', 'Parametro deve ser do tipo inteiro'));
    }

    const jobOportunitie = await JobOportunity.findByPk(id);
    if (!jobOportunitie) {
      errors.push(new FieldMessage('id', 'Oportunidade de emprego inexistente'));
    }

    const schema = Yup.object().shape({
      name: Yup.string().required('Campo Obrigatório'),
      company: Yup.object().shape({
        id: Yup.number().integer().required('Campo Obrigatório'),
      }).required('Campo Obrigatório'),
    });

    try {
      await schema.validate(req.body, { abortEarly: false });
    } catch (err) {
      err.inner.forEach((error) => {
        errors.push(new FieldMessage(error.path, error.message));
      });
      return errors;
    }

    const companyInDatabase = await Company.findByPk(company.id);
    if (!companyInDatabase) {
      errors.push(new FieldMessage('company.id', 'Empresa não existe'));
    }

    return errors;
  };

  validations.deleteJobOportunitie = async (req) => {
    const errors = [];
    const { id } = req.params;

    const jobOportunitie = await JobOportunity.findByPk(id);
    if (!jobOportunitie) {
      errors.push(new FieldMessage('id', 'Oportunidade de emprego inexistente'));
    }

    return errors;
  };

  return validations;
};
